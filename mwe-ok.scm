
(define-syntax overides
  (syntax-rules ()
    ((_ 1 (name pname ...) (method ...))
     (define-syntax name
       (syntax-rules (more go)
	 ((_ more mname mmethod)
	  (overides 1 (mname name pname ...) (method ... mmethod)))
	 ((_ go)
	  (letrec* ((full-path '(name pname ...))
		    (method (lambda () (print full-path 'method))) ...)
	    (method) ...)))))
    ((_ name method)
     (overides 1 (name) (method)))))

(overides one test)
(one more two test)
(print "===")
(one go)
(print "===")
(two go)
