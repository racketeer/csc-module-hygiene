#!/bin/sh


echo ======== OK:
csi -q -b mwe-ok.scm
echo

echo ======== Compilation:
csc -P -J mwe-ko-common.scm
csc -P -J mwe-ko-one.scm
csc -P -J mwe-ko-two.scm
echo

echo ======== KO:
csi -q -b mwe-ko-run.scm
